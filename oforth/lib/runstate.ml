open Core

(* open Wordlist *)
(* open Rawword *)
(* open Utils *)

(* Runtime stack *)
type stack = string array
let create_stack = Utils.create_string_array

(* The current run state *)
type t = {
 stack: stack;
 dict: Rawword.t array;
 wordlist: Wordlist.t;
}
let create = {
  stack = create_stack;
  dict = Rawword.create_array;
  wordlist = Wordlist.create
 }

let wordlist {stack=_; dict=_; wordlist=wl} = wl

let inject_words { stack=s; dict=d; wordlist=ws1 } =
  { stack=s; dict=d; wordlist=Array.append ws1 ws}

 : t -> Wordlist.t -> t
