
(* open Runstate *)

(* Internally defined words *)
type internal_handler = (Runstate.t -> Runstate.t)
type internal_def = { name: string; handler: internal_handler }
type t = internal_def array

let internal_def_to_name { name=n; handler=_ } = n
let internal_def_to_handler { name=_; handler=h } = h

(* Internal handlers *)

let internal_dot rs = rs

(* Create the initial (populated) array of internal dictionary words *)
let create = Array.of_list [ { name="."; handler=internal_dot } ]
