open Core

let create_string_array = 
  Array.init 0 ~f:(fun _ -> "")
