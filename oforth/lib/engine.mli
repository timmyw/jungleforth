
(** Process a state *)
val process : State.state -> State.state
