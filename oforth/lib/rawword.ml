open Core
open Utils

(* A word defined by code *)
type t = { name: string; def: string array }
let create_array =
  Array.init 0 ~f:(fun _ -> {name = ""; def = Utils.create_string_array})
