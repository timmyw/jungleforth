
(** Current runstate *)
type t

(** Create an initial runstate *)
val create : t

(** Retrieve the wordlist from a runstate *)
val wordlist : t -> Wordlist.t

(** Inject a list of words into the supplied runstate *)
val inject_words : t -> Wordlist.t -> t

