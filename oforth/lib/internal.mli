
(** Internal dictionary *)
type t

(** Create a populated internal dictionary *)
val create : t
