(* open Core *)
(* open Runstate *)
(* open Internal *)

(* The full run state *)
type t = {
  runstate: Runstate.t;
  internal_dict: Internal.t;
 }

let state_runstate { runstate=rs; internal_dict=_ } = rs

let create = { runstate = Runstate.create; internal_dict = Internal.create }

let inject_words { runstate = rs; internal_dict = id } ws = 
  { runstate = Runstate.inject_words rs ws; internal_dict = id }

(* let get_stack { s = stack; d = _; ws = _ } = stack *)
