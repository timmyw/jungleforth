(** The word list type *)
type t

(** Create an empty word list *)
val create : t
