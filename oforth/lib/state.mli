
(** Current state *)
type t

(** Generate a blank initial state *)
val create : t
