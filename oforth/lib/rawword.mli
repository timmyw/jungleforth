
(** Raw word type *)
type t

(** Create an empty array of raw words *)
val create_array : t array
