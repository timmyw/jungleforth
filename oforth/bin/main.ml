open Core.Std

(*
   read in a line
   split into words
   process first
   state
*)

let process_line s l = 
  let words = String.split ~on:' ' line in
  Oforth.Engine.process 

let process_input s =
  try while true do
    let line = input_line stdin in
    match process_line s line with
    | Some error -> Printf.printf "%s\n" error
    | None -> ()
  done with End_of_file -> ()

let () =
  let s = Oforth.State.create in
  process_input s
