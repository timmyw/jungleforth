#+TITLE: jungleforth

A naive implementation of Forth, written in C++17, making good use of
Boost.
