
#include "boost/variant.hpp"
#include <iostream>
#include <stack>
#include <vector>
#include <algorithm>

#include <readline/readline.h>
#include <readline/history.h>

#define STACKUNDERFLOW(cnt)						\
        if (global_stack.size() < cnt) { std::cerr << "Stack underflow" << std::endl; return; }

#define POP(n)                                                                 \
  STACKUNDERFLOW(1);                                                           \
  auto n = global_stack.top();                                                 \
  global_stack.pop();

#define PUSH(v) global_stack.push(v);

typedef boost::variant<int, float, std::string, double> VARIANT;
typedef std::pair<std::string, std::vector<VARIANT>> sub_dict;
std::stack<VARIANT> global_stack;

typedef void (*inbuilt_handler)();
typedef std::map<std::string, inbuilt_handler> inbuilt_map;

bool compiling = false;
bool str_mode = false;

void ib_emit() {
    POP(v);

    int i = boost::get<int>(v);
    char tmp[5];
    sprintf(tmp, "%c", i);
    std::cout << tmp;
}

void ib_pop_n_show() {
    POP(v);
    std::cout << v;
}

template <typename T>
void traverse_stack(std::stack<T> & st) {
    if(st.empty())
        return;
    T x = st.top();
    std::cout << x << " ";
    st.pop();
    traverse_stack(st);
    st.push(x);
}

void ib_stack() {
    traverse_stack<VARIANT>(global_stack);
}

void ib_drop() {
    POP(v);
}

void ib_dup() {
    POP(v);
    global_stack.push(v);
    global_stack.push(v);
}

extern inbuilt_map inbuilt;

void ib_dict() {
    // built ins first
    for (auto i : inbuilt) {
	std::cout << i.first << " ";
    }

    std::cout << std::endl;
}

void ib_swap() {
    POP(v1);
    POP(v2);
    PUSH(v1);
    PUSH(v2);
}

void ib_over() {
    POP(v1); // a
    POP(v2); // b

    PUSH(v1);
    PUSH(v2);
    PUSH(v1);
}

void ib_rot() {
    POP(v1);
    POP(v2);
    POP(v3);

    PUSH(v1);
    PUSH(v3);
    PUSH(v2);
}

void ib_plus() { POP(v1); POP(v2); int r = boost::get<int>(v1) + boost::get<int>(v2); VARIANT v3(r); PUSH(v3); }
void ib_sub() { POP(v1); POP(v2); int r = boost::get<int>(v1) - boost::get<int>(v2); VARIANT v3(r); PUSH(v3); }
void ib_mult() { POP(v1); POP(v2); int r = boost::get<int>(v1) * boost::get<int>(v2); VARIANT v3(r); PUSH(v3); }
void ib_div() { POP(v1); POP(v2); int r = boost::get<int>(v1) / boost::get<int>(v2); VARIANT v3(r); PUSH(v3); }
void ib_mod() { POP(v1); POP(v2); int r = boost::get<int>(v1) % boost::get<int>(v2); VARIANT v3(r); PUSH(v3); }
void ib_remmod() {
    POP(v1);
    POP(v2);
    int d = boost::get<int>(v1) % boost::get<int>(v2);
    int r = boost::get<int>(v1) - d * boost::get<int>(v2);
    VARIANT v3(r); PUSH(v3);
    VARIANT v4(d); PUSH(v4);
}

void ib_quit() { exit(0); }

inbuilt_map inbuilt{
    { "emit", ib_emit }
    , { ".", ib_pop_n_show }
    , { ".s", ib_stack }
    , { ".q", ib_quit }
    , { "drop", ib_drop }
    , { "dup", ib_dup }
    , { "dict", ib_dict }
    , { "swap", ib_swap }
    , { "over", ib_over }
    , { "rot", ib_rot }
    , { "+", ib_plus }
    , { "-", ib_sub }
    , { "*", ib_mult }
    , { "/", ib_div }
    , { "mod", ib_mod }
    , { "/mod", ib_remmod }
};

bool try_inbuilts(std::string &c) {
  inbuilt_map::iterator i;
  if ((i = inbuilt.find(c)) != inbuilt.end()) {
    i->second();
    return true;
  }
  return false;
}

bool is_number(const std::string& str)
{
    for (char const &c : str) {
        if (std::isdigit(c) == 0) return false;
    }
    return true;
}

bool is_number_float(const std::string& str)
{
    for (char const &c : str) {
        if (std::isdigit(c) == 0 && c != '.') return false;
    }
    if (str.length() == 1) return false;
    return true;
}

std::vector<std::string> get_input() {
  std::vector<std::string> words;

  char prompt[129];
  snprintf(prompt, 128, "[%ld]%s%s ",
	   global_stack.size(),
	   compiling ? "[c]" : "",
	   str_mode ? "[\"]" : "");
  char* line = readline(prompt);
  if (!line || !*line)
    return words;
  add_history(line);

  std::string cline(line);
  free(line);

  size_t pos = 0;
  std::string word;
  const std::string delim = " ";
  while ((pos = cline.find(delim)) != std::string::npos) {
    word = cline.substr(0, pos);
    cline.erase(0, pos + delim.length());
    words.push_back(word);
  }
  if (cline.length() != 0) {
    // Save the last word
    words.push_back(cline);
  }
  return words;
}

void run_command(const std::string &cmd) {
  
}

int main(int argc, char *argv[]) {
    std::string str_build = "";
    std::cout << "jungleforth" << std::endl;
    while (1) {
	auto i = get_input();
	while (i.size() >= 1) {
	    auto command = i[0];
	    if (command == ":") {	   // Start compiling
	    } else if (command == ";") { // End compiling
		
	    } else if (command == "s\"") { // String capture
		str_build = "";
		str_mode = true;
	    } else if (command.find('\"') == command.size()-1) {
		// End of string capture
		str_mode = false;
		str_build += command.substr(0, command.length()-1);
		PUSH(VARIANT(str_build));
	    } else {			     // Not compiling
		auto handled = false;
		if (str_mode) {
		    str_build += command;
		    handled = true;
		}
		if (!handled && compiling) {
		}
		
		if (!handled && is_number(command)) {
		    VARIANT v(atoi(command.c_str()));
		    global_stack.push(v);
		    handled = true;
		}
		if (is_number_float(command)) {
		    VARIANT v(atof(command.c_str()));
		    global_stack.push(v);
		    handled = true;
		}

		if (!handled) { // Try inbuilts
		    handled = try_inbuilts(command);
		    if (!handled) {
			std::cerr << "Unrecognized word:" << command << std::endl;
		    }
		}
	    }
	    
	    i.erase(i.begin());
	}
    }
}

